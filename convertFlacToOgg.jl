import Distributed

const HELP = "Script can take several arguments.
    Mandatory are:
    -s={sourceDir}: Base directory to look for subfolders with files to convert.
    Optional are:
    -t={targetDir}: The target directory to safe the conversion result. Default is '/tmp'
    -q={quality}: Quality for encoding to ogg. Default is 6. Possible values between 1 (very low) and 10 (very high)
"
const DEFAULT_QUALITY = "6"
const DEFAULT_TARGETDIR = "/tmp"

struct ScriptArguments
    sourceDir::String
    targetDir::String
    quality::String
end

struct IODirectories
    sourceDirectory
    targetDirectory
end

function processScriptArguments()::ScriptArguments
    sourceDir::String = ""
    targetDir::String = DEFAULT_TARGETDIR
    quality::String = DEFAULT_QUALITY
    for arg in ARGS
        splitted = split(arg, "=")
        if (splitted[1] == "-s")
            sourceDir = splitted[2]
        elseif (splitted[1] == "-t")
            targetDir = splitted[2]
        elseif (splitted[1] == "-q")
            quality = splitted[2]
        end
    end
    return ScriptArguments(sourceDir, targetDir, quality)
end


function processQuality(qualityAsStr)::Int16
    qualityAsNumber = tryparse(Int16, qualityAsStr)
    qualityAsInt::Int16 = 0
    if qualityAsNumber === nothing
        println("Given quality value not numeric. Using default value: $DEFAULT_QUALITY")
        qualityAsInt = parse(Int16, DEFAULT_QUALITY)
    else
        if qualityAsNumber < 1
            println("Given quality value too small, using 1 instead.")
            qualityAsInt = 1
        elseif qualityAsNumber > 10
            println("Given quality value too big, using 10 instead.")
            qualityAsInt = 10
        else
            qualityAsInt = qualityAsNumber
        end
    end
    return qualityAsInt
end


function createIODirs(args::ScriptArguments)::IODirectories
    if isempty(args.sourceDir) || isempty(args.targetDir) || !isdir(args.sourceDir) || !isdir(args.targetDir)
        println("Illegal source or target dir.")
        println(HELP)
        exit(1)
    else
        return IODirectories(args.sourceDir, args.targetDir)
    end 
end

function createSubDirectory(root::String, dir::String, sourceDir::String, targetDir::String)
    tmpDir = joinpath(root, dir)
    subDirPath = split(tmpDir, sourceDir)
    tmpTargetDir = joinpath(targetDir, subDirPath[2])
    mkpath(tmpTargetDir)
end

function getPaths(root::String, file::String, sourceDir::String, targetDir::String)::Tuple{String,String}
    filePath = joinpath(root, file)
    subDirPath = split(dirname(filePath), sourceDir)
    tmpTargetFilePath = joinpath(targetDir, subDirPath[2], file)
    return filePath, tmpTargetFilePath
end

function createGStreamerCommand(tmpTargetFilePath::String, filePath::String, gstreamerQuality::Float64)::Base.AbstractCmd
    prefix = split(tmpTargetFilePath, ".flac")
    targetFileName = prefix[1] * ".ogg"
    convert = `gst-launch-1.0 
                -q
                uridecodebin 
                uri="file://$filePath" 
                ! audioconvert 
                ! vorbisenc 
                quality=$gstreamerQuality 
                ! oggmux 
                ! filesink 
                location=$targetFileName` 
end

function transcode(ioDirs::IODirectories, quality::Int16)
    sourceDir = joinpath(ioDirs.sourceDirectory, "")
    gstreamerQuality = quality / 10

    local functionArray::AbstractArray{Function} = []

    for (root, dirs, files) in walkdir(sourceDir)
        for dir in dirs
            createSubDirectory(root, dir, sourceDir, ioDirs.targetDirectory)
        end
        for file in files
            filePath, tmpTargetFilePath = getPaths(root, file, sourceDir, ioDirs.targetDirectory) 
        
            if (endswith(lowercase(file), "flac"))
                transcodeCmd = createGStreamerCommand(tmpTargetFilePath, filePath, gstreamerQuality)
                transcode() = run(transcodeCmd)
                push!(functionArray, transcode)
            else
                copy() = cp(filePath, tmpTargetFilePath; force=true)
                push!(functionArray, copy)
            end
        end
    end

    @sync Distributed.@distributed for f in functionArray
        f()
    end
end

function main()
    args::ScriptArguments = processScriptArguments()
    quality::Int16 = processQuality(args.quality)
    ioDirs::IODirectories = createIODirs(args)
    transcode(ioDirs, quality)
end

main()