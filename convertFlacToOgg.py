import click
import os
from os.path import join, split
import subprocess
import shutil
import threading, multiprocessing, queue

cpu_count = multiprocessing.cpu_count()
q = queue.Queue()

def worker():
    while True:
        cmd = q.get()
        cmd()
        q.task_done()

def createTargetDir(root: str, dir: str, sourcedir:str, targetdir:str):
    """ Creates the subdirectory in the target directory
    """

    tmpDir=join(root,dir)
    subDirPath=os.path.relpath(tmpDir,sourcedir)
    subTargetDir=os.path.join(targetdir, subDirPath)
    os.mkdir(subTargetDir)

def createGStreamerCmd(filePath: str, targetFileName: str, quality: float) -> list:
    cmd = ['gst-launch-1.0', "-q", 'uridecodebin',
    'uri=file://'+filePath,
    "!", "audioconvert",
    "!", "vorbisenc",
    "quality="+str(quality),
    "!" ,"oggmux",
    "!" ,"filesink",
    "location="+targetFileName]
    return cmd

def getPaths(root: str, filename: str, sourcedir: str, targetdir: str) -> tuple:
    filePath=os.path.join(root, filename)
    subDirPath = os.path.relpath(filePath,sourcedir)
    tmpTargetFilePath=os.path.join(targetdir, subDirPath)
    return filePath, tmpTargetFilePath

class TransformationTask:
    """
    A callable wrapping the transformation of the source file to the target file.
    """

    filePath = ""
    tmpTargetFilePath = ""
    gstreamerQuality = ""

    def __init__(self, filePath: str, tmpTargetFilePath: str, gstreamerQuality: float):
        self.filePath = filePath
        self.tmpTargetFilePath = tmpTargetFilePath
        self.gstreamerQuality = gstreamerQuality

    def __call__(self):
        if(self.filePath.lower().endswith("flac")):
            basename, suffix = os.path.splitext(self.tmpTargetFilePath)
            targetFileName = basename + ".ogg"
            cmd = createGStreamerCmd(self.filePath,
                    targetFileName,
                    self.gstreamerQuality)
            subprocess.run(cmd, capture_output=False)
        else:
            shutil.copy(self.filePath, self.tmpTargetFilePath)


@click.command()
@click.argument('sourcedir', type=click.Path(exists=True))
@click.option('--targetdir', '-t', default='/tmp', type=click.Path(exists=True),
              help="""The target directory to safe the conversion result.
                  Default is '/tmp'""")
@click.option('--quality', '-q', default=6,type=click.IntRange(1, 10),
              help="""Quality for encoding to ogg. Default is 6.
                  Possible values between 1 (very low) and 10 (very high)""")


def main(sourcedir: str, targetdir: str, quality: int):
    """Program to convert flac files in the given SOURCEDIR to ogg files."""
    gstreamerQuality=quality/10
    tasks = []
    for root, dirs, files in os.walk(sourcedir):
        for dir in dirs:
            createTargetDir(root, dir, sourcedir, targetdir)
        for filename in files:
            filePath, tmpTargetFilePath = getPaths(root, filename, sourcedir, targetdir)
            task = TransformationTask(filePath, tmpTargetFilePath, gstreamerQuality)
            tasks.append(task)

    for i in range(cpu_count):
        threading.Thread(target=worker, daemon=True).start()

    for task in tasks:
        q.put(task)
    q.join()

if __name__ == '__main__':
    main()


